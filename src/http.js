/**
 * http.js
 *
 * This module exports the function which deploys an http server with an
 * Express webapp bound.
 */
const http = require('http');

// simple logging function
function log(s) { process.stdout.write(`${s}\n`); }

// the function which deploys the server
module.exports = function (app, options) {
  // deal with options
  if (typeof options !== 'object') { options = {}; }

  // set port for the app
  const port = options.port || 8666;
  let availablePort = port;
  app.set('port', availablePort);

  // create the http server
  const server = http.createServer(app);

  // handle port errors by scooting up a port
  server.on('error', function (err) {
    let portRetries = options.portRetries;
    if (typeof portRetries === 'undefined') { portRetries = 10; }
    if (err.code === 'EADDRINUSE' && availablePort - port < portRetries) {
      availablePort += 1;
      server.listen(availablePort);
      return;
    }
    throw err;
  });

  // log server details when all works
  server.on('listening', function () {
    log('='.repeat(80));
    if (options.name) {
      log(options.name);
    }
    log('Server is listening on port ' + server.address().port);
    log('='.repeat(80));
  });

  // start the server
  server.listen(availablePort);

  // return
  return server;
};
