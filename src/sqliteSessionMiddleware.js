/**
 * sqliteSessions.js
 *
 * This module creates middleware for storing user sessions in a sqlite database
 */
const session = require('express-session');
const connectSQL = require('connect-sqlite3');

// create sqlite3 session store
const SQLiteStore = connectSQL(session);

// middleware
module.exports = function (options) {
  if (typeof options === 'string') { options = { secret: options }; }
  else if (typeof options !== 'object' || typeof options.secret !== 'string') {
    throw Error(
      'User must provide string in options field "secret" for session storage.'
    );
  }
  return (
    session({
      secret: options.secret,
      saveUninitialized: false,
      resave: false,
      cookie: { maxAge: 1000 * 60 * 60 * 24 * 30 },
      store: new SQLiteStore({
        dir: options.sqliteDir || process.cwd(),
        db: options.sqliteName || 'database.db',
      }),
    })
  )
};
