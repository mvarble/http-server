/**
 * app.js
 *
 * This module exports the Express webapp
 */
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');

module.exports = function (options) {
  // deal with options
  if (typeof options !== 'object') { options = {}; }

  // create the Express app
  const app = express();

  // body-parser
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // logger middleware
  app.use(morgan('tiny'));

  // static middleware
  let static = options.static;
  if (typeof static === 'undefined' ) { 
    static = {
      path: '/static',
      src: path.join(process.cwd(), 'dist'),
      close: true,
    }
  }
  if (static.path && static.src) {
    app.use(static.path, express.static(static.src));
    if (static.close) {
      app.use(static.path + '/*', (req, res) => res.status(404).send());
    }
  }

  // return!
  return app;
}
