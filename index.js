/**
 * This is the entry point to our module. It exports the functions exported 
 * at src/app.js and src/http.js
 */
module.exports = {
  createApp: require('./src/app'),
  createHTTP: require('./src/http'),
  sqliteSessionMiddleware: require('./src/sqliteSessionMiddleware')
};
