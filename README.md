# HTTP Server

This is an npm package which generates Express apps with middleware I commonly use, along with HTTP servers to serve them.
The module exports an object with the following properties.

## createApp

This is a function which returns an Express app with associated middleware.

### Signature

```js
  const expressApp = createApp(); // Express app with the defaults
  const anotherExpressApp = createApp({
      static: {
          path: '/static', 
          src: process.cwd() + '/dist', 
          close: true 
      },
  });
```

The function takes in an optional object argument, which can have the following keys.

- **static**: This allows us to have HTTP routes corresponding to static files in the filesystem. To turn this functionality off, provide `static: {}`. Otherwise, this is an object with the following keys.
  - **path**: This is a string corresponding to the HTTP route of the static directory. Default `'static'`.
  - **src**: This is a string corresponding to the path of the static directory in the filesystem. Default `'$PROJECT_DIR/dist'`.
  - **close**: This is a boolean corresponding to if we want HTTP routes not corresponding to files in the filesystem to to 404. Default `true`.

## createHTTP

This is a function which starts a server on an available port and returns it.

### Signature

```js
  const server = createServer();
  const ratServer = createServer({ port: 8666, portRetries: 10, name: 'Rat' });
```

The function takes in an optional object argument, which can have the following keys.

- **port**: This is a number corresponding to the port the server is on. Default `8666`.
- **portRetries**: This is a nonnegative integer corresponding to how many times we increase the port number and retry on `EADDRINUSE` errors. Default `10`.
- **name**: This is a string that literally only serves for displaying in the log. Default `undefined`.

## sqliteSessionMiddleware

This is a function which returns middleware for connecting session storage to a sqlite database.

### Signature

```js
  someExpressApp.use(sqliteSessionMiddleware({
      secret: 'thesecretkeyisRATS',
      sqliteDir: process.cwd(),
      sqliteName: 'database.db'
  }));
  anotherExpressApp.use(sqliteSessionMiddleware('thesecretkeyisRATS'));
```

The function takes in a single argument that is either a string or an object.
The string would take place of the field **secret** in the object with the following fields.

- **secret (required)**: This is a string corresponding to the secret passphrase for session storage.
- **sqliteDir**: This is a string corresponding to the directory in which the sqlite database file is stored. Default `$PROJECT_DIR`.
- **sqliteName**: This is a string corresponding to the filename of the sqlite database. Default is `'database.db'`.
